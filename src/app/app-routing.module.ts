import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from './login/login.component';

import { AuthGuardService as AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  {
    path: '', redirectTo: 'control', pathMatch: 'full'
  },
  {
    path: "login", component: LoginComponent
  },
  {
    path: 'control', component: HomeComponent,
    // canActivate: [AuthGuard],
    children: [
      { path: '', loadChildren: () => import('../app/control/control.module').then(m => m.ControlModule)},
    ]
  },
  {path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

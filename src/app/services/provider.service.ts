import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {
  readonly rootUrl = 'http://localhost:7100';
  // readonly rootUrl = 'http://localhost:7200';
  // readonly rootUrl = 'http://103.129.14.92:7100';
  // readonly rootUrl = 'http://154.202.2.242:7100';

  constructor(
    private http: HttpClient,
  ) { }

  serviceRequest(requestobject: any) {
    var reqHeader = new HttpHeaders({
      // Authorization: "Bearer " + localStorage.getItem("TAgritech-Token"),
      // Authorization: "Bearer " + this.cookie.getCookie("TAgritech-Token"),
      "Content-Type": "application/json; charset=utf-8"
    });

    switch (requestobject.method) {
      case "GET":
        return this.http.get(this.rootUrl + requestobject.url, {
          headers: reqHeader
        });
      case "POST":
        return this.http.post(
          this.rootUrl + requestobject.url,
          requestobject.data,
          { headers: reqHeader }
        );
      case "PUT":
        return this.http.put(
          this.rootUrl + requestobject.url,
          requestobject.data,
          { headers: reqHeader }
        );
      case "DELETE":
        return this.http.delete(this.rootUrl + requestobject.url, {
          headers: reqHeader
        });
      default:
        break;
    }

    return
  }
}

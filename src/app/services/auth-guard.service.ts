import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CookieService } from './cookie.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
  isLoggedin: boolean = false;
  socialUser: any;

  constructor(
    public router: Router,
    private cookie: CookieService,
    private socialAuthService: SocialAuthService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    var cookiedata = this.cookie.getCookie("socialAuthService")
    var socialdata = JSON.parse(cookiedata || '{}')


    var datenow = new Date(); // Your timezone!
    var epochnow = datenow.getTime();
 
    
    if (socialdata.response.expires_at < epochnow) {
      console.log("Timeout");
      this.router.navigate(['login'])
      return false
    }
    else {
      console.log("Time alive");
      return true
    }
  }

  // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
  //   console.log(this.socialAuthService.authState);

  //   return this.socialAuthService.authState.pipe(
  //     map((socialUser: SocialUser) => !!socialUser),
  //     tap((isLoggedIn: boolean) => {
  //       console.log("-------------");

  //       if (!isLoggedIn) {
  //         this.router.navigate(['login']);
  //       }
  //     })
  //   );
  // }
}

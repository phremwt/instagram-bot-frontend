import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { ProvidersService } from '../services/provider.service';
import { CookieService } from '../services/cookie.service';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css'],
})
export class ControlComponent implements OnInit {
  itemsPerPage: number = 30
  currentPage: number = 1
  p: number = 1;
  userList: any = [];
  displayUserList: any = [];
  userToFollow: string = '';
  mediaShortCode: string = '';

  inProcess: boolean = false;
  processStage: any
  inProcessMessage: any
  statusProcessBackend: boolean = false
  cancelOrderProcess: boolean = false
  stopProcessCause: string = '';
  titleSnackBar: any
  appBackendStart: boolean = false

  processCount: number = 0
  successCount: number = 0
  errorCount: number = 0
  totalCount: number = 0
  checkingCount: number = 0
  errorLoginAmount: number = 0

  selectListData: string = "All"
  numSelectListData: any
  targetProcess: any
  checkEnableTarget: boolean = false
  mediaMode: string = 'follow';

  constructor(
    private providersService: ProvidersService,
    private cookieService: CookieService
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit() {
    (async () => {
      await this.getBackendStatus()

      this.mediaMode = this.cookieService.getCookie('mediaMode') as string
      this.userToFollow = this.cookieService.getCookie('userToFollow') as string
      this.mediaShortCode = this.cookieService.getCookie('mediaShortCode') as string

      await this.delay(10000);
      if (this.statusProcessBackend) {
        this.processMonitoring()
        this.titleSnackBar = "Backgroound has process running..."
        this.showSnackBar()
      }

    })()
  }

  selectMediaOption(media: any) {
    this.mediaMode = media;
    this.getBackendStatus()
    this.processMonitoring()
  }

  fileChangeEvent(event: any): void {
    this.userList = [];
    // // this.imageChangedEvent = event;

    // this.displayHowToUse = false
    const file = event.target.files[0];

    if (file) {
      this.inProcess = false;

      var reader = new FileReader();
      reader.readAsText(file);
      reader.onload = (e) => {
        let csv = reader.result;

        let allTextLines = (<string>csv).toString().split(/\r|\n|\r/);
        let headers = allTextLines[0].split(',');

        for (var i = 1; i < allTextLines.length; i++) {
          var obj = {};
          var data = allTextLines[i].split(',');

          if (data.length === headers.length) {
            var rowdata = {
              username: data[0],
              password: data[1],
              status: 'waiting',
            };
            this.userList.push(rowdata);
          }
        }
        this.displayUserList = this.userList
        this.totalCount = this.displayUserList.length;
      };
    }
  }

  optionSelectData(data: string) {
    this.selectListData = data
    this.displayUserList = []

    if (this.selectListData == "All") {
      this.numSelectListData = ''
      this.displayUserList = this.userList
      this.totalCount = this.displayUserList.length;
      return
    }

    if (!isNaN(this.numSelectListData) && (this.numSelectListData < this.userList.length)) {
      switch (this.selectListData) {
        case 'First':
          for (var i = 0; i < this.numSelectListData; i++) {
            this.displayUserList.push(this.userList[i])
          }
          break;
        case 'Last':
          var istart = (this.userList.length - parseInt(this.numSelectListData))
          for (var i = istart; i < this.userList.length; i++) {
            this.displayUserList.push(this.userList[i])
          }
          break;
        case 'Random':
          var arr = this.randomUniqueNum(this.userList.length, this.numSelectListData)
          for (var i = 0; i < arr.length; i++) {
            this.displayUserList.push(this.userList[arr[i] - 1])
          }
          break;
        default:
          break;
      }
      this.totalCount = this.displayUserList.length;
    }

    // if (this.numSelectListData) {

    // } else {
    //   return
    // }
  }

  randomUniqueNum(range: number, outputCount: number) {
    let arr = []
    for (let i = 1; i <= range; i++) {
      arr.push(i)
    }

    let result = [];

    for (let i = 1; i <= outputCount; i++) {
      const random = Math.floor(Math.random() * (range - i));
      result.push(arr[random]);
      arr[random] = arr[range - i];
    }

    return result;
  }

  updateStatus(data: any) {
    for (var item in data) {
      this.userList.push(data[item]);
    }
  }

  async processMonitoring() {
    while (this.statusProcessBackend) {
      await this.getBackendStatus()
      await this.delay(10000);
    }
  }

  delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  showSnackBar() {
    var snack = document.getElementById("snackbar") as HTMLElement

    // Add the "show" class to DIV
    snack.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function () { snack.className = snack.className.replace("show", ""); }, 4500);
  }


  getSuccessList() {
    var successlist = [];
    var successstatus;
    if (this.mediaMode == 'follow') {
      successstatus = 'following';
    } else {
      successstatus = 'liked';
    }

    for (var item in this.userList) {
      if (this.userList[item].status == successstatus) {
        successlist.push(this.userList[item]);
      }
    }

    return successlist;
  }

  getErrorList() {
    var errorlist = [];
    var successstatus;
    if (this.mediaMode == 'follow') {
      successstatus = 'following';
    } else {
      successstatus = 'liked';
    }
    for (var item in this.userList) {

      if (this.userList[item].status != successstatus && this.userList[item].status != 'waiting') {
        errorlist.push(this.userList[item]);
      }
    }

    return errorlist;
  }

  downloadCSV(args: any) {
    (async () => {
      var data, filename, link, csv;
      var errorlist = this.displayUserList;
      console.log(errorlist);
      
      csv = this.convertArrayOfObjectsToCSV({
        data: errorlist,
      });

      if (csv == null) return;

      filename = args.filename || 'export.csv';

      if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
      }
      data = encodeURI(csv);

      link = document.createElement('a');
      link.setAttribute('href', data);
      link.setAttribute('download', filename);
      link.click();
    })();
  }

  convertArrayOfObjectsToCSV(args: any) {
    var result: string,
      ctr: number,
      keys: any[],
      columnDelimiter: string | undefined,
      lineDelimiter: string,
      data;

    data = args.data || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = args.columnDelimiter || ',';

    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function (item: { [x: string]: any }) {
      ctr = 0;
      keys.forEach(function (key: string | number) {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });
    return result;
  }

  process() {
    this.statusProcessBackend = true
    this.processCount = 0
    this.successCount = 0
    this.errorCount = 0
    // this.stopProcessCause = ""
    this.userList = this.displayUserList
    this.cancelOrderProcess = false

    if (this.mediaMode == 'follow') {
      this.cookieService.setCookie("userToFollow", this.userToFollow, 1)
      this.follow();
    } else {
      this.cookieService.setCookie("mediaShortCode", this.mediaShortCode, 1)
      this.likeMedia();
    }

    this.cookieService.setCookie('mediaMode', this.mediaMode, 1)
  }

  enableTarget(data: any) {
    this.checkEnableTarget = data.target.checked
    if (!this.checkEnableTarget) {
      this.targetProcess = undefined
    }
  }

  openModal(key: any) {
    var modal = document.getElementById(key) as HTMLElement
    modal.style.display = "block";
  }

  closeModal(key: any) {
    var modal = document.getElementById(key) as HTMLElement
    modal.style.display = "none";
  }


  follow() {
    if (this.userList.length > 0 && this.userToFollow != undefined) {
      var data = {
        usertofollow: this.userToFollow,
        userlist: this.userList,
        target: this.targetProcess
      };

      var url = '/api/instagram/follow';

      var requestObject = {
        url: url,
        method: 'POST',
        data: data,
      };

      this.serviceRequest(requestObject, 'follow');
    } else {
      return;
    }
  }

  likeMedia() {
    if (this.userList.length > 0 && this.mediaShortCode != undefined) {
      var data = {
        mediashortcode: this.mediaShortCode,
        userlist: this.userList,
        target: this.targetProcess
      };

      var url = '/api/instagram/like';

      var requestObject = {
        url: url,
        method: 'POST',
        data: data,
      };

      this.serviceRequest(requestObject, 'likeMedia');
    } else {
      return;
    }
  }

  userInfo() {
    var data = {
      username: this.userToFollow,
    };

    var url = '/api/instagram/userinfo';

    var requestObject = {
      url: url,
      method: 'POST',
      data: data,
    };

    this.serviceRequest(requestObject, 'userInfo');
  }

  clearBackendProcess() {
    if (!this.statusProcessBackend) {

      this.userToFollow = ''
      this.mediaShortCode = ''
      this.cookieService.setCookie("userToFollow", '', 1)
      this.cookieService.setCookie("mediaShortCode", '', 1)

      var url = '/api/instagram/backend/clear';

      var requestObject = {
        url: url,
        method: 'POST',
        data: null
      };

      this.serviceRequest(requestObject, 'clearBackendProcess');
    }
  }

  mediaInfo() {
    // console.log(this.croppedImage);

    var data = {
      username: this.userToFollow,
    };

    var url = '/api/instagram/mediainfo/' + 'CYcO1vXhhZR';

    var requestObject = {
      url: url,
      method: 'GET',
    };

    this.serviceRequest(requestObject, 'mediaInfo');
  }

  getUserList() {
    var url = '/api/instagram/userlist';

    var requestObject = {
      url: url,
      method: 'GET',
    };

    this.serviceRequest(requestObject, 'getUserList');
  }

  getBackendStatus() {
    var url = '/api/instagram/backend/status';

    var requestObject = {
      url: url,
      method: 'GET',
    };

    this.serviceRequest(requestObject, 'getBackendStatus');
  }

  cencelOrder() {
    this.openModal('modal-loading')
    var url = '/api/instagram/backend/cancel';

    var requestObject = {
      url: url,
      method: 'POST',
      data: null
    };

    this.serviceRequest(requestObject, 'cencelOrder');
  }

  getAppStartStatus() {
    var url = '/api/instagram/backend/appstart';

    var requestObject = {
      url: url,
      method: 'GET',
    };

    this.serviceRequest(requestObject, 'getAppStartStatus');
  }


  serviceRequest(requestobject: any, callback: string): any {
    this.providersService.serviceRequest(requestobject)?.subscribe(
      (data: any) => {
        switch (callback) {
          case 'follow':
            this.inProcess = true;
            this.processMonitoring();
            break;
          case 'likeMedia':
            this.inProcess = true;
            this.processMonitoring();
            break;
          case 'userInfo':
            console.log(data.userid);

            break;
          case 'mediaInfo':
            console.log(data.founduser);

            break;
          case 'getUserList':
            if (data.userlist && (data.userlist.length > 0)) {
              this.userList = [];

              this.userList = data.userlist;
              this.displayUserList = this.userList
              this.totalCount = this.displayUserList.length;
              // console.log(this.displayUserList);
              
              this.processCount = data.processcount;
              this.successCount = this.getSuccessList().length;
              this.errorCount = this.getErrorList().length;

              this.inProcess = data.inprocess
              this.processStage = data.processstage
              this.inProcessMessage = data.inprocessmessage
              this.inProcessMessage = data.inprocessmessage
              this.checkingCount = data.checkingcount
              this.errorLoginAmount = data.errorloginamount
            }

            break;
          case 'getBackendStatus':
            this.statusProcessBackend = data.inprocess
            if (this.statusProcessBackend) {
              this.mediaMode = data.activity
            } 

            if (this.cancelOrderProcess) {
              this.userList = []
              this.displayUserList = this.userList
            }
           
            if (data.activity == this.mediaMode) {
              this.getUserList()
            }

            // if (data && data.stopprocesscause) {
            //   this.stopProcessCause = data.stopprocesscause
            // }
            break;

          case 'clearBackendProcess':
            this.userList = []
            this.displayUserList = []
            this.totalCount = 0
            this.processCount = 0
            this.successCount = 0
            this.errorCount = 0
            this.getBackendStatus
            break;
          case 'getAppStartStatus':
            this.appBackendStart = data.appstart
            break;
          case 'cencelOrder':
            (async () => {
              // await this.delay(10000)

              while (this.appBackendStart == false) {
                await this.getAppStartStatus()
                await this.delay(3000)
              }

              this.closeModal('modal-loading')
              this.cancelOrderProcess = true
              this.appBackendStart = false
              await this.getBackendStatus
        
            })()
            break;
          default:
            break;
        }
      },
      (err: HttpErrorResponse) => {
        // console.log(err);
      }
    );
  }
}
